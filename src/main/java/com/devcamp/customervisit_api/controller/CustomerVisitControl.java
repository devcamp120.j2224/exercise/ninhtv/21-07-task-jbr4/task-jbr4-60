package com.devcamp.customervisit_api.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisit_api.model.Customer;
import com.devcamp.customervisit_api.model.Visit;

@RestController
public class CustomerVisitControl {
    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> getListVisit(){
        Customer customer1 = new Customer("Tony");
        Customer customer2 = new Customer("Peter");
        Customer customer3 = new Customer("Jack");
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        Visit visit3 = new Visit(customer3, new Date());
        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println(visit3);

        ArrayList<Visit> lisVisits = new ArrayList<>();
        lisVisits.add(visit1);
        lisVisits.add(visit2);
        lisVisits.add(visit3);
        return lisVisits;
    }
}
